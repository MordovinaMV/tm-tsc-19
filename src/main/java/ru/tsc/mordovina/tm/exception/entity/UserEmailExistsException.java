package ru.tsc.mordovina.tm.exception.entity;

import ru.tsc.mordovina.tm.exception.AbstractException;

public class UserEmailExistsException extends AbstractException {

    public UserEmailExistsException(final String email) {
        super("Error. User with email `" + email + "` already exists.");
    }

}
