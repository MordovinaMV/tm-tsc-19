package ru.tsc.mordovina.tm.command.task;

import ru.tsc.mordovina.tm.command.AbstractTaskCommand;
import ru.tsc.mordovina.tm.util.TerminalUtil;

public class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return "project-clear";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Drop all projects";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Description");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getTaskService().create(name, description);
    }

}
