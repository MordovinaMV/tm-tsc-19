package ru.tsc.mordovina.tm.command;

import ru.tsc.mordovina.tm.exception.empty.EmptyNameException;
import ru.tsc.mordovina.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.mordovina.tm.model.Project;
import ru.tsc.mordovina.tm.model.Task;

import java.util.List;

public abstract class AbstractProjectTaskCommand extends AbstractCommand {

    protected void showProjectTasks(Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus());
        List<Task> tasks = serviceLocator.getProjectTaskService().findTaskByProjectId(project.getId());
        for (Task task : tasks) {
            System.out.println("Id: " + task.getId());
            System.out.println("Name: " + task.getName());
            System.out.println("Description: " + task.getDescription());
            System.out.println("Status: " + task.getStatus());
        }
    }

    protected Project add(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return new Project(name, description);
    }

}
