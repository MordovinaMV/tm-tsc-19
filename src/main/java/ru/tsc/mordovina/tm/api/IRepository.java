package ru.tsc.mordovina.tm.api;

import ru.tsc.mordovina.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void add(final E entity);

    void remove(final E entity);

    void clear();

    List<E> findAll();

    List<E> findAll(Comparator<E> comparator);

    E findById(final String id);

    E findByIndex(final Integer index);

    E removeById(final String id);

    E removeByIndex(final Integer index);

    Integer getSize();

    boolean existsById(final String id);

    boolean existsByIndex(final Integer index);

}
