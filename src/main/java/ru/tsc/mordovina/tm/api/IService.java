package ru.tsc.mordovina.tm.api;

import ru.tsc.mordovina.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

}
