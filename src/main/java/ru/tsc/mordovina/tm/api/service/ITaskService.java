package ru.tsc.mordovina.tm.api.service;

import ru.tsc.mordovina.tm.api.IService;
import ru.tsc.mordovina.tm.enumerated.Status;
import ru.tsc.mordovina.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IService<Task> {

    void create(String name);

    void create(String name, String description);

    Task findByName(String name);

    Task findByIndex(Integer index);

    Task removeByName(String name);

    Task removeByIndex(Integer index);

    Task updateById(final String id, final String name, final String index);

    Task updateByIndex(final Integer index, final String name, final String description);

    boolean existsByIndex(int index);

    boolean existsByName(String name);

    Task startById(String id);

    Task startByIndex(Integer index);

    Task startByName(String name);

    Task finishById(String id);

    Task finishByIndex(Integer index);

    Task finishByName(String name);

    Task changeStatusById(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

    Task changeStatusByName(String name, Status status);

}
