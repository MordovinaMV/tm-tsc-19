package ru.tsc.mordovina.tm.api.repository;

import ru.tsc.mordovina.tm.api.IRepository;
import ru.tsc.mordovina.tm.enumerated.Status;
import ru.tsc.mordovina.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    boolean existsByName(String name);

    Project findByName(String name);

    Project removeByName(String name);

    Project startById(String id);

    Project startByIndex(Integer index);

    Project startByName(String name);

    Project finishById(String id);

    Project finishByIndex(Integer index);

    Project finishByName(String name);

    Project changeStatusById(String id, Status status);

    Project changeStatusByIndex(Integer index, Status status);

    Project changeStatusByName(String name, Status status);

}
