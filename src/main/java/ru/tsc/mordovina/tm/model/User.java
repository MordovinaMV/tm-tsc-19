package ru.tsc.mordovina.tm.model;

import ru.tsc.mordovina.tm.enumerated.Role;
import ru.tsc.mordovina.tm.util.HashUtil;

import java.util.UUID;

public class User extends AbstractEntity {

    private String login;

    private String password;

    private String email;

    private String lastName;

    private String firstName;

    private String middleName;

    private Role role = Role.USER;

    public User() {
    }

    public User(String login, String password) {
        this.login = login;
        this.password = HashUtil.salt(password);
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

}