package ru.tsc.mordovina.tm.model;

import ru.tsc.mordovina.tm.api.entity.IWBS;
import ru.tsc.mordovina.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public class Task extends AbstractEntity implements IWBS {

    private String name = "";
    private String description = "";
    private Status status = Status.NOT_STARTED;
    private String projectId = null;
    private Date startDate;
    private Date finishDate;
    private Date created = new Date();

    public Task() {
    }

    public Task(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public Date getStartDate() {
        return startDate;
    }

    @Override
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

}
