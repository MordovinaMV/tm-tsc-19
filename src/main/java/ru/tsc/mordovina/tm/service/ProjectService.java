package ru.tsc.mordovina.tm.service;

import ru.tsc.mordovina.tm.api.repository.IProjectRepository;
import ru.tsc.mordovina.tm.api.service.IProjectService;
import ru.tsc.mordovina.tm.enumerated.Status;
import ru.tsc.mordovina.tm.exception.empty.*;
import ru.tsc.mordovina.tm.exception.system.IndexIncorrectException;
import ru.tsc.mordovina.tm.model.Project;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }


    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public Project findByName(final String name) {
        if (name == null || name.isEmpty()) throw new IndexIncorrectException();
        return projectRepository.findByName(name);
    }

    @Override
    public Project updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = repository.findById(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) {
        if (index == null) throw new EmptyIndexException();
        if (index < 0 || index > repository.getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = repository.findByIndex(index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public boolean existsByIndex(final int index) {
        return repository.existsByIndex(index);
    }

    @Override
    public boolean existsByName(final String name) {
        return projectRepository.existsByName(name);
    }

    @Override
    public Project startById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.startById(id);
    }

    @Override
    public Project startByIndex(final Integer index) {
        if (index == null) throw new EmptyIndexException();
        if (index < 0 || index > projectRepository.getSize()) throw new IndexIncorrectException();
        return projectRepository.startByIndex(index);
    }

    @Override
    public Project startByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.startByName(name);
    }

    @Override
    public Project finishById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.finishById(id);
    }

    @Override
    public Project finishByIndex(final Integer index) {
        if (index == null) throw new EmptyIndexException();
        if (index < 0 || index > projectRepository.getSize()) throw new IndexIncorrectException();
        return projectRepository.finishByIndex(index);
    }

    @Override
    public Project finishByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.finishByName(name);
    }

    @Override
    public Project changeStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        return projectRepository.changeStatusById(id, status);
    }

    @Override
    public Project changeStatusByIndex(final Integer index, final Status status) {
        if (index == null) throw new EmptyIndexException();
        if (index < 0 || index > projectRepository.getSize()) throw new IndexIncorrectException();
        if (status == null) throw new EmptyStatusException();
        return projectRepository.changeStatusByIndex(index, status);
    }

    @Override
    public Project changeStatusByName(final String name, final Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        return projectRepository.changeStatusByName(name, status);
    }

}