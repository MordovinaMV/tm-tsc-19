package ru.tsc.mordovina.tm.service;

import ru.tsc.mordovina.tm.api.IRepository;
import ru.tsc.mordovina.tm.api.IService;
import ru.tsc.mordovina.tm.exception.empty.*;
import ru.tsc.mordovina.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.mordovina.tm.exception.system.IndexIncorrectException;
import ru.tsc.mordovina.tm.model.AbstractEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    protected final IRepository<E> repository;

    public AbstractService(final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public void add(final E entity) {
        if (entity == null) throw new ProjectNotFoundException();
        repository.add(entity);
    }

    @Override
    public void remove(final E entity) {
        if (entity == null) throw new ProjectNotFoundException();
        repository.remove(entity);
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public List<E> findAll(Comparator<E> comparator) {
        if (comparator == null) return Collections.emptyList();
        return repository.findAll(comparator);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public E findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Override
    public E findByIndex(final Integer index) {
        if (index == null) throw new EmptyIndexException();
        if (index < 0 || index > repository.getSize()) throw new IndexIncorrectException();
        return repository.findByIndex(index);
    }

    @Override
    public boolean existsById(String id) {
        if (id == null) throw new EmptyIdException();
        return repository.existsById(id);
    }

    @Override
    public boolean existsByIndex(Integer index) {
        if (index == null) throw new EmptyIndexException();
        if (index > getSize() || index < 0) throw new IndexIncorrectException();
        return repository.existsByIndex(index);
    }

    @Override
    public E removeById(String id) {
        if (id == null) throw new EmptyIdException();
        return repository.removeById(id);
    }

    @Override
    public E removeByIndex(final Integer index) {
        if (index == null) throw new EmptyIndexException();
        if (index > getSize() || index < 0) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

    @Override
    public Integer getSize() {
        return repository.getSize();
    }

}
