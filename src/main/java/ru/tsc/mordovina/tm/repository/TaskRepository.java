package ru.tsc.mordovina.tm.repository;

import ru.tsc.mordovina.tm.api.IRepository;
import ru.tsc.mordovina.tm.api.repository.ITaskRepository;
import ru.tsc.mordovina.tm.enumerated.Status;
import ru.tsc.mordovina.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllTaskByProjectId(String projectId) {
        List<Task> listByProject = new ArrayList<>();
        for (Task task : list)
            if (projectId.equals(task.getProjectId()))
                listByProject.add(task);
        return listByProject;
    }

    @Override
    public boolean existsByName(final String name) {
        return findByName(name) != null;
    }

    @Override
    public Task bindTaskToProjectById(final String projectId, final String taskId) {
        final Task task = findById(taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskById(final String taskId) {
        final Task task = findById(taskId);
        task.setProjectId(null);
        return task;
    }

    @Override
    public void removeAllTaskByProjectId(final String projectId) {
        List<Task> listByProject = findAllTaskByProjectId(projectId);
        for (Task task : listByProject)
            list.remove(task);
    }

    @Override
    public Task findByName(final String name) {
        for (Task task : list)
            if (name.equals(task.getName())) return task;
        return null;
    }

    @Override
    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public Task startById(final String id) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByIndex(final Integer index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishById(final String id) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByIndex(final Integer index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task changeStatusById(String id, Status status) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(Integer index, Status status) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByName(String name, Status status) {
        final Task task = findByName(name);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

}
